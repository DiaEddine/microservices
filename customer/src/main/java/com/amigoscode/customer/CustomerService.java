package com.amigoscode.customer;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@AllArgsConstructor
public class CustomerService  {
    
    private final CustomerRepository customerRepository;
    private final RestTemplate restTemplate;
    
    public void registerCustomer(CustomerRegistrationRequest customerRegistrationRequest) {
        Customer customer = Customer.builder()
                .firstName(customerRegistrationRequest.firstName())
                .lastName(customerRegistrationRequest.lastName())
                .email(customerRegistrationRequest.email()).build();
        customerRepository.save(customer);
        //TODO: CHECK IF EMAIL VALID
        //TODO: CHECK IF EMAIL NOT TAKEN
        // todo check if fraudester
        FraudCheckResponse fraudCheckResponse = restTemplate.getForObject(
                "http://localhost:8082/api/v1/fraud-check/{customersID}",
                FraudCheckResponse.class,
                customer.getId()
        );
        if (fraudCheckResponse.isFraudster())
            throw new IllegalStateException("fraudster");
        //todo send notifican
    }
}
