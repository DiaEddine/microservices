package com.amigoscode.fraud;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;


@Service
public class FraudCheckHistoryService {
    
    private final FraudCheckHistoryRepository fraudCheckHistoryRepository;

    public FraudCheckHistoryService(FraudCheckHistoryRepository fraudCheckHistoryRepository) {
        this.fraudCheckHistoryRepository = fraudCheckHistoryRepository;
    }

    public boolean isFraudulentCustomer(Integer id){
            fraudCheckHistoryRepository.save(FraudCheckHIstory.builder()
                            .customerId(id)
                    .isFraudster(false)
                            .created(LocalDateTime.now())
                    .build());
            return false;
        }
}
