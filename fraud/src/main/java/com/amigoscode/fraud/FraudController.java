package com.amigoscode.fraud;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("api/v1/fraud-check")
@Slf4j
public class FraudController {
    
    private final FraudCheckHistoryService fraudCheckHistoryService;
    

    @GetMapping("{customerID}")
    public FraudCheckResponse isFraudster(@PathVariable("customerID") Integer customerID){
        log.info("fraud check request for customer {}", customerID);
        return new FraudCheckResponse(fraudCheckHistoryService.isFraudulentCustomer(customerID));
    }
    
}
